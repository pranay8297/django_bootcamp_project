from celery import shared_task
import os
import json
from bootcamp.qa.models import Question
from .celery import app


@app.task
def download_all_questions(user_id):
    import time; time.sleep(1)
    file_name = str(user_id) + '.json'
    homedir = os.getcwd()
    file_path = os.path.join(homedir, file_name)
    questions = Question.objects.filter(user=user_id)
    question_list = []
    for i in questions:
        ques = {}
        ques['title'] = i.title
        ques['content'] = i.content
        question_list.append(ques)
    # print(question_list)
    with open(file_path, 'w+', encoding='utf-8') as f:
        json.dump(question_list, f)
