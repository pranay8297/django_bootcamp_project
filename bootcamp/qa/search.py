from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import DocType, Text, Date, Search
from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch
from . import models




connections.create_connection(
   hosts=['https://elastic:b3E0L0ADI1IeV0RKwtyvKaqA@11e8a73a2d7945e5aceb5c6336e700c0.ap-southeast-1.aws.found.io:9243'], timeout=20)


class QuestionIndex(DocType):
   user = Text()
   timestamp = Date()
   title = Text()   
   content = Text()
   id = Text()

   class Index:
       name = 'question-index'


def bulk_indexing():
   for b in models.Question.objects.all():
       b.indexing()


def search(query):
   s = Search().filter("query_string", query='*'+query+'*', fields=['title', 'content', 'tags'])
   response = s.execute()
   id_list = []
   for i in response:
      print(i['id'])
      print(1)
      id_list.append(i['id'])
   print(id_list)
   return id_list
