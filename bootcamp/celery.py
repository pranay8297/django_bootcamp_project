from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from config.settings import base, local
# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings.base')

app = Celery('bootcamp', backend='amqp', broker='redis://localhost:6379/')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(base.INSTALLED_APPS)


@app.task(bind=True)
def debug_task(self):
   print('Request: {0!r}'.format(self.request))
