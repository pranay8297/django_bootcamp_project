FROM python:3.6-slim
ENV PYTHONDONTWRITEBYTECODE boot_buffer
ENV PYTHONUNBUFFERED boot_buffer
RUN mkdir /code
WORKDIR /code
COPY . /code
RUN apt-get update
RUN pip install --upgrade pip
RUN apt install -y libpq-dev python-dev
RUN apt install -y build-essential
RUN apt install -y postgresql-server-dev-all
RUN pip install psycopg2	
RUN pip install -r /code/abc.txt

EXPOSE 8000
#completed the basic setup... shift to the next thing

